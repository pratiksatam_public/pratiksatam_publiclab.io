---
layout: page
title: Publications
permalink: /Publications/
---

**Journal Publications**
1.	Gao X., Ram S., Philip R., Rodriguez J., Szep J., Shao S., Satam P., Pacheco J., Hariri S., “Selecting Post-Processing Schemes for Accurate Detection of Small Objects in Low-Resolution Wide-Area Aerial Imagery”, Remote Sens. 2022, 14, 255. https://doi.org/10.3390/rs14020255.
2.	Satam, Shalaka, Pratik Satam, Jesus Pacheco, and Salim Hariri. "Security framework for smart cyber infrastructure." Cluster Computing (2021): 1-12.
3.	Wu, Chongke, Sicong Shao, Cihan Tunc, Pratik Satam, and Salim Hariri. "An explainable and efficient deep learning framework for video anomaly detection." Cluster Computing(2021): 1-23.
4. Satam Pratik, Hariri Salim, “WIDS: An Anomaly Based Intrusion Detection System for Wi-Fi (IEEE 802.11) Protocol”, IEEE Transactions on Networks and Service Management, 2020, 2373-7379.  
5. Gao, Xin, Jeno Szep, Pratik Satam, Salim Hariri, Sundaresh Ram, and Jeffrey J. Rodriguez. "Spatio-Temporal Processing for Automatic Vehicle Detection in Wide-Area Aerial Video." arXiv preprint arXiv:2010.14025 (2020).  
6. AlShawi, Amany, Pratik Satam, Firas Almoualem, and Salim Hariri. "Effective Wireless Communication Architecture for Resisting Jamming Attacks." IEEE Access (2020).
7. Pacheco, Jesus, Victor H. Benitez, Luis C. Félix-Herrán, and Pratik Satam. "Artificial Neural Networks-Based Intrusion Detection System for Internet of Things Fog Nodes." IEEE Access 8 (2020): 73907-73918.  
8. Skaria, Rinku, Pratik Satam, and Zain Khalpey. "Opportunities and Challenges of Disruptive Innovation in Medicine Using Artificial Intelligence." The American Journal of Medicine(2020), Elsevier.  
9. Pacheco, Jesus, Cihan Tunc, Pratik Satam, and Salim Hariri. "Secure and resilient cloud services for enhanced living environments." IEEE Cloud Computing 3, no. 6 (2016): 44-52.  
10. Satam, Pratik, Hamid Alipour, Youssif B. Al-Nashif, and Salim Hariri. "Anomaly Behavior Analysis of DNS Protocol." J. Internet Serv. Inf. Secur. 5, no. 4 (2015): 85-97.  
11. Alipour, Hamid, Youssif B. Al-Nashif, Pratik Satam, and Salim Hariri. "Wireless anomaly detection based on IEEE 802.11 behavior analysis." IEEE transactions on information forensics and security 10, no. 10 (2015): 2158-2170.  
  
**Book Chapters**  
1. Pratik Satam, Shalaka Satam, Salim Hariri, and Amany Alshawi Anomaly, “Behavior Analysis of IoT Protocols.”,  in “Modeling and Design of Secure Internet of Things.”, Editors: Drs. Charles A Kamhoua, Laurent Njilla, Alexander Kott, and Sachin Shetty, IEEE Press, 2020.  
  
**Conference Publications**  
1.	Shao, Sicong, Pratik Satam, Shalaka Satam, Khalid Al-Awady, Gregory Ditzler, Salim Hariri, and Cihan Tunc. "Multi-Layer Mapping of Cyberspace for Intrusion Detection." In 2021 IEEE/ACS 18th International Conference on Computer Systems and Applications (AICCSA), pp. 1-8. IEEE, 2021.
2.	Torres A., Satam P., Bose T., “Machine Learning Classifiers for Anomaly Based Wireless Intrusion Detection”, International Telemetry Conference, 2021.
3.	Satam, Shalaka, Pratik Satam, and Salim Hariri. "Multi-level Bluetooth Intrusion Detection System." In 2020 IEEE/ACS 17th International Conference on Computer Systems and Applications (AICCSA), pp. 1-8. IEEE, 2020.
4.	Yao, Likai, Cihan Tunc, Pratik Satam, and Salim Hariri. "Resilient Machine Learning (rML) Ensemble Against Adversarial Machine Learning Attacks." In International Conference on Dynamic Data Driven Application Systems, pp. 274-282. Springer, Cham, 2020
5.	Satam, Pratik, Shalaka Satam, and Salim Hariri. "Bluetooth intrusion detection system (BIDS)." In 2018 IEEE/ACS 15th International Conference on Computer Systems and Applications (AICCSA), pp. 1-7. IEEE, 2018.
6.	Hess, Samuel, Pratik Satam, Gregory Ditzler, and Salim Hariri. "Malicious HTML File Prediction: A Detection and Classification Perspective with Noisy Data." In 2018 IEEE/ACS 15th International Conference on Computer Systems and Applications (AICCSA), pp. 1-7. IEEE, 2018.
7.	Satam, Pratik. "Anomaly Based Wi-Fi Intrusion Detection System." In 2017 IEEE 2nd International Workshops on Foundations and Applications of Self* Systems (FAS* W), pp. 377-378. IEEE, 2017.
8.	Satam, Pratik, Jesus Pacheco, Salim Hariri, and Mohommad Horani. "Autoinfotainment security development framework (ASDF) for smart cars." In 2017 International Conference on Cloud and Autonomic Computing (ICCAC), pp. 153-159. IEEE, 2017.
9.	Almoualem, Firas, Pratik Satam, Jang-Geun Ki, and Salim Hariri. "SDR-Based Resilient Wireless Communications." In 2017 International Conference on Cloud and Autonomic Computing (ICCAC), pp. 114-119. IEEE, 2017.
10.	Shao, Sicong, Cihan Tunc, Pratik Satam, and Salim Hariri. "Real-time irc threat detection framework." In 2017 IEEE 2nd International Workshops on Foundations and Applications of Self* Systems (FAS* W), pp. 318-323. IEEE, 2017.
11.	Pacheco, Jesus, Cihan Tunc, Pratik Satam, and Salim Hariri. "Secure and resilient cloud services for enhanced living environments." IEEE Cloud Computing 3, no. 6 (2016): 44-52.
12.	Satam, Pratik, Douglas Kelly, and Salim Hariri. "Anomaly behavior analysis of website vulnerability and security." In 2016 IEEE/ACS 13th International Conference of Computer Systems and Applications (AICCSA), pp. 1-7. IEEE, 2016.
13.	Hariri, Salim, Cihan Tunc, Pratik Satam, Firas Al-Moualem, and Erik Blasch. "Dddas-based resilient cyber battle management services (d-rcbms)." In Proceedings of the 2015 IEEE 22nd International Conference on High Performance Computing Workshops (HiPCW), pp. 65-65. 2015.
14.	Tunc, Cihan, Salim Hariri, Fabian De La Peña Montero, Farah Fargo, and Pratik Satam. "CLaaS: Cybersecurity Lab as a Service--design, analysis, and evaluation." In 2015 International Conference on Cloud and Autonomic Computing, pp. 224-227. IEEE, 2015.
15.	Satam, Pratik, Hamid Alipour, Youssif Al-Nashif, and Salim Hariri. "Dns-ids: Securing dns in the cloud era." In 2015 International Conference on Cloud and Autonomic Computing, pp. 296-301. IEEE, 2015.
16.	Tunc, Cihan, Salim Hariri, Fabian De La Peña Montero, Farah Fargo, Pratik Satam, and Youssif Al-Nashif. "Teaching and Training Cybersecurity as a Cloud Service." In 2015 International Conference on Cloud and Autonomic Computing, pp. 302-308. IEEE, 2015.
17.	Satam, Pratik. "Cross layer Anomaly based intrusion detection system." In 2015 IEEE International Conference on Self-Adaptive and Self-Organizing Systems Workshops, pp. 157-161. IEEE, 2015. 
