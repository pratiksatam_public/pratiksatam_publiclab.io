---
layout: page
title: Datasets
permalink: /Datasets/
---


1. **Autonomous Vehicle Global Positioning System (GPS) security dataset**  
GPS normal behavior and spoofing dataset collected at the University of Arizona.  
[Dataset Link](https://gitlab.com/gps-dataset/autonomous-vehicle-gps-security-dataset)  
Cite: XYZ
